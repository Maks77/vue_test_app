import Vue from 'vue'
import axios from 'axios'

import Util from './Util'

let bus = new Vue({
    data() {
        return {
            mainData: {
                dataForRender: {}
            }
        }
    },
    created() {
        this.getRemoutData();
    },

    methods: {
        getRemoutData() {
            let url = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed/?url=https://habr.com/'
            axios
                .get(url)
                .then(response=>{
                    if (response.status === 200) {
                        console.log('response', response)
                        this.getDataForRender(response.data);
                    }
                })
        },

        getDataForRender(data) {
            console.log('inner data', data)
            let auditRefs = data.lighthouseResult.categories.performance.auditRefs
            let audits = data.lighthouseResult.audits;
    
            //console.log('auditRefs', auditRefs)
            console.log('audits', audits)
    
            const opportunityAudits = 
                auditRefs
                    .filter(audit => {
                        return audit.group === 'load-opportunities' && !Util.showAsPassed(audits[audit.id])
                    })
                    .sort((auditA, auditB) => {
                        return Util.getWastedMs(audits[auditB.id]) - Util.getWastedMs(audits[auditA.id]);
                    })
            //console.log('opportunityAudits', opportunityAudits)
            
            const diagnosticAudits = 
                auditRefs
                    .filter(audit => audit.group === 'diagnostics' && !Util.showAsPassed(audits[audit.id]))
                    .sort((a, b) => {
                      const scoreA = audits[a.id].scoreDisplayMode === 'informative' ? 100 : Number(audits[a.id].score);
                      const scoreB = audits[b.id].scoreDisplayMode === 'informative' ? 100 : Number(audits[b.id].score);
                      return scoreA - scoreB;
                    });
    
            //console.log('diagnosticAudits', diagnosticAudits);
    
            const passedAudits = 
                auditRefs
                    .filter(audit => (audit.group === 'load-opportunities' || audit.group === 'diagnostics') &&
                        Util.showAsPassed(audits[audit.id]));
    
            //console.log('passedAudits', passedAudits)
            
    
            let dataForRender = {
                opportunities: {
                    title: 'Opportunities',
                    subtitle: 'Approximate savings',
                    data: []
                },
                diagnostics: {
                    title: 'Diagnostics',
                    subtitle: '- Details of the performance of your application',
                    data: []
                },
                passed: {
                    title: 'Passed Audits',
                    subtitle: 'Successful Audits',
                    data: []
                }
            }
    
            opportunityAudits.forEach(el=>{
                let res = audits[el.id];
                if (res !== undefined) {
                    dataForRender.opportunities.data.push(res)
                }
            })
    
            diagnosticAudits.forEach(el=>{
                let res = audits[el.id];
                if (res !== undefined) {
                    dataForRender.diagnostics.data.push(res)
                }
            })
    
            passedAudits.forEach(el=>{
                let res = audits[el.id];
                if (res !== undefined) {
                    dataForRender.passed.data.push(res)
                }
            })


            this.$emit('onDataReceived', dataForRender);
        }
    }


})

export { bus }